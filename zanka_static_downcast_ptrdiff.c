#include "libzanka_private.h"

ZANKA_PURE ptrdiff_t zanka_static_downcast_ptrdiff(const zanka_type_t *src, const zanka_type_t *dest) {
    ptrdiff_t i;
    for (i = 0; i < src->n_supers; i++)
        if (src->supers[i].type == dest)
            break;
    if (i == src->n_supers)
        return -1;
    return src->supers[i].offset + 2 * sizeof(void *);
}
