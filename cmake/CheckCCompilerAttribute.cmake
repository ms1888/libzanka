include_guard(GLOBAL)
include(CheckCCompilerFlag)
include(CheckCSourceCompiles)

macro(CHECK_C_COMPILER_ATTRIBUTE _ATTRIBUTE _RESULT)
    check_c_compiler_flag("-Werror=attributes" _CAN_WERROR_ATTRIBUTES)
    set(SAFE_CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS}")
    if(_CAN_WERROR_ATTRIBUTES)
        set(CMAKE_REQUIRED_FLAGS "-Werror=attributes")
    else(_CAN_WERROR_ATTRIBUTES)
        set(CMAKE_REQUIRED_FLAGS)
    endif(_CAN_WERROR_ATTRIBUTES)
    check_c_source_compiles("${_ATTRIBUTE} int somefunc() { return 0; }
        int main(void) { return somefunc(); }" ${_RESULT})
    set(CMAKE_REQUIRED_FLAGS "${SAFE_CMAKE_REQUIRED_FLAGS}")
endmacro(CHECK_C_COMPILER_ATTRIBUTE)
