#include "libzanka.h"

#include <stdlib.h>

void zanka_free(void *obj) {
    free(*((void **)obj - 1));
}
