#ifndef LIBZANKA_PRIVATE_H
#define LIBZANKA_PRIVATE_H

#include "libzanka.h"

typedef struct {
    size_t offset;
    const zanka_type_t *type;
} zanka_type_parent_t;

struct _zanka_type {
    const char *class_name;
    size_t size;
    zanka_type_parent_t *supers;
    int n_supers;
    void (*destructor)(void *);
};

#endif
