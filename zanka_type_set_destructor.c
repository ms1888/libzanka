#include "libzanka_private.h"

void zanka_type_set_destructor(zanka_type_t *t, void (*ptr)()) {
    t->destructor = (void (*)(void *))ptr;
}
