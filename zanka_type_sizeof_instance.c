#include "libzanka_private.h"

ZANKA_PURE size_t zanka_type_sizeof_instance(const zanka_type_t *t) {
    zanka_type_parent_t *last = &t->supers[t->n_supers-1];
    return last->offset + 2 * sizeof(void *) + last->type->size;
}
