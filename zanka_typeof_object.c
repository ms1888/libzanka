#include "libzanka.h"

ZANKA_PURE zanka_type_t* zanka_typeof_object(void *obj) {
    return **((zanka_type_t ***)obj - 1);
}
