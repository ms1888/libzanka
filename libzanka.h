#ifndef LIBZANKA_H
#define LIBZANKA_H

#include "zanka_attr.h"

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _zanka_type zanka_type_t;

ZANKA_WARN_UNUSED_RESULT void *zanka_alloc(const zanka_type_t *t);
void zanka_free(void *obj);
ZANKA_PURE zanka_type_t *zanka_typeof_object(void *obj);
ZANKA_PURE size_t zanka_type_sizeof_instance(const zanka_type_t *t);
ZANKA_PURE void *zanka_typecast(void *obj, const zanka_type_t *t);

ZANKA_PURE ptrdiff_t zanka_static_cast_ptrdiff(const zanka_type_t *src, const zanka_type_t *dest);
ZANKA_PURE ptrdiff_t zanka_static_downcast_ptrdiff(const zanka_type_t *src, const zanka_type_t *dest);

void zanka_type_set_destructor(zanka_type_t *t, void (*ptr)());
void zanka_destroy(void *obj);

ZANKA_WARN_UNUSED_RESULT zanka_type_t *zanka_inherit(const zanka_type_t *parent, size_t size);
ZANKA_WARN_UNUSED_RESULT zanka_type_t *zanka_inherit2(const zanka_type_t *parent1, const zanka_type_t *parent2, size_t size);

#ifdef __cplusplus
}
#endif

#endif
