#include "libzanka.h"

ZANKA_PURE void *zanka_typecast(void *obj, const zanka_type_t *t) {
    if (!obj)
        return NULL;
    if (*((void **)obj - 2) == t)
        return obj;
    zanka_type_t *type = zanka_typeof_object(obj);
    char *raw = *((void **)obj - 1);
    ptrdiff_t offset;
    offset = zanka_static_downcast_ptrdiff(type, t);
    if (offset != -1)
        return raw + offset;
    return NULL;
}
