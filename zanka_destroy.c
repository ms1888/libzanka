#include "libzanka_private.h"

void zanka_destroy(void *obj) {
    zanka_type_t *t = zanka_typeof_object(obj);
    for (int i = 0; i < t->n_supers; i++) {
        zanka_type_parent_t *p = &t->supers[i];
        void (*destructor)(void *) = p->type->destructor;
        if (destructor)
            destructor((char *)*((void **)obj - 1) + p->offset);
    }
    zanka_free(obj);
}
