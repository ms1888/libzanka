#include "libzanka_private.h"

#include <stdlib.h>

ZANKA_WARN_UNUSED_RESULT void *zanka_alloc(const zanka_type_t *t) {
    char *raw = calloc(1, zanka_type_sizeof_instance(t));
    for (int i = 0; i < t->n_supers; i++) {
        zanka_type_parent_t *p = t->supers + i;
        const void **ptr = (const void **)(raw + p->offset);
        ptr[0] = p->type;
        ptr[1] = raw;
    }
    return raw + 2 * sizeof(void *);
}
