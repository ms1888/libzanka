#include "libzanka_private.h"

#include <stdlib.h>

ZANKA_WARN_UNUSED_RESULT zanka_type_t *zanka_inherit2(const zanka_type_t *parent1, const zanka_type_t *parent2, size_t size) {
    zanka_type_t *t = malloc(sizeof *t);
    t->destructor = NULL;
    t->n_supers = parent1->n_supers + parent2->n_supers + 1;
    for (int i = 0; i < parent1->n_supers; i++)
        for (int j = 0; j < parent2->n_supers; j++)
            if (parent1->supers[i].type == parent2->supers[j].type)
                t->n_supers--;
    t->supers = malloc(t->n_supers * sizeof(zanka_type_parent_t));
    t->supers[0].type = t;
    t->supers[0].offset = 0;
    t->size = size;
    for (int i = 1, j = 0; j < parent1->n_supers; i++, j++) {
        t->supers[i].type = parent1->supers[j].type;
        t->supers[i].offset = t->supers[i-1].offset + 2 * sizeof(void *) + t->supers[i-1].type->size;
    }
    for (int i = parent1->n_supers + 1, j = 0; j < parent2->n_supers; j++) {
        for (int k = 0; k < parent1->n_supers; k++)
            if (parent1->supers[k].type == parent2->supers[j].type)
                continue;
        t->supers[i].type = parent2->supers[j].type;
        t->supers[i].offset = t->supers[i-1].offset + 2 * sizeof(void *) + t->supers[i-1].type->size;
        i++;
    }
    return t;
}
