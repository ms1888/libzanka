#include "libzanka_private.h"

#include <stdlib.h>

ZANKA_WARN_UNUSED_RESULT zanka_type_t *zanka_inherit(const zanka_type_t *parent, size_t size) {
    zanka_type_t *t = malloc(sizeof *t);
    t->destructor = NULL;
    t->n_supers = parent ? parent->n_supers + 1 : 1;
    t->supers = malloc(t->n_supers * sizeof(zanka_type_parent_t));
    t->supers[0].type = t;
    t->supers[0].offset = 0;
    t->size = size;
    for (int i = 1, j = 0; i < t->n_supers; i++, j++) {
        t->supers[i].type = parent->supers[j].type;
        t->supers[i].offset = t->supers[i-1].offset + 2 * sizeof(void *) + t->supers[i-1].type->size;
    }
    return t;
}
